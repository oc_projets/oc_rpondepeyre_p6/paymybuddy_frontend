import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { AccountModel } from '../model/AccountModel';
import { CurrentUserModel } from '../model/CurrentUserModel';
import { TransactionModel } from '../model/TransactionModel';
import { UserModel } from '../model/UserModel';

export const USERENV: string = 'http://localhost:8080/user/';
export const TRANSENV: string = 'http://localhost:8080/transfer/';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  token = sessionStorage.getItem('token');
  constructor(private http: HttpClient) {
    this.updateCurrentUser();
  }

  public currentUser = new BehaviorSubject<CurrentUserModel>({
    id: 0,
    mail: '',
    lastName: '',
    firstName: '',
    relations: [],
    banques: [],
    transactions: [],
    wallet: 0,
  });

  updateCurrentUser() {
    this.getCurrenUser().subscribe(value => this.currentUser.next(value));
  }

  getCurrenUser(): Observable<CurrentUserModel> {
    return this.http
      .get<CurrentUserModel>(USERENV)
      .pipe(
        tap(value =>
          value.relations.forEach(
            element =>
              (element.fullName = element.firstName + ' ' + element.lastName)
          )
        )
      );
  }

  getUserbyMail(mail: string): Observable<UserModel> {
    let param = new HttpParams().append('mail', mail);
    return this.http.get<UserModel>(USERENV + 'find', { params: param });
  }

  addUserAsFriend(mail: string): Observable<CurrentUserModel> {
    let param = new HttpParams().set('mail', mail);
    return this.http.get<CurrentUserModel>(USERENV + 'addfriend', {
      params: param,
    });
  }
  removeFriend(mail: string): Observable<CurrentUserModel> {
    let param = new HttpParams().set('mail', mail);
    return this.http.get<CurrentUserModel>(USERENV + 'removefriend', {
      params: param,
    });
  }

  addBankAccount(bank: AccountModel): Observable<CurrentUserModel> {
    return this.http.post<CurrentUserModel>(USERENV + 'addbank', bank);
  }
  removeBankAccount(id: number): Observable<CurrentUserModel> {
    let param = new HttpParams().set('bankid', id);
    return this.http.get<CurrentUserModel>(USERENV + 'removebank', {
      params: param,
    });
  }

  deposit(bank: AccountModel, amount: Number): Observable<CurrentUserModel> {
    return this.http.post<CurrentUserModel>(USERENV + 'deposit', {
      bank,
      amount,
    });
  }

  withdraw(bank: AccountModel, amount: Number): Observable<CurrentUserModel> {
    return this.http.post<CurrentUserModel>(USERENV + 'withdraw', {
      bank,
      amount,
    });
  }

  transferMoney(transaction: TransactionModel): Observable<CurrentUserModel> {
    return this.http.post<CurrentUserModel>(TRANSENV, transaction);
  }

  getCommission(): Observable<number> {
    return this.http.get<number>(TRANSENV + 'commission');
  }
}
