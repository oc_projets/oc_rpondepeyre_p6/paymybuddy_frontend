import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  Router,
  RouterStateSnapshot,
  UrlTree,
} from '@angular/router';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { LoginService } from './LoginService';

@Injectable({
  providedIn: 'root',
})
export class LoggedInGuard implements CanActivate {
  constructor(
    private loginService: LoginService,
    private router: Router,
    private messageservice: MessageService
  ) {}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    if (this.loginService.islogged()) {
      return true;
    } else {
      this.loginService.logout();
      this.router.navigate(['/login']);
      this.messageservice.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'Your authentication has expired',
      });
      return false;
    }
  }
}
