import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private router: Router, private messageservice: MessageService) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    let authReq = req.clone();
    let token = sessionStorage.getItem('token');
    if (authReq.url.includes('/auth')) {
      return next.handle(authReq);
    } else {
      authReq = req.clone({
        headers: req.headers.set('Authorization', 'Bearer ' + token),
      });
      return next.handle(authReq).pipe(
        catchError((error: HttpErrorResponse): Observable<any> => {
          if (error.status === 401) {
            sessionStorage.clear();
            this.router.navigate(['/login']);
            this.messageservice.add({
              severity: 'error',
              summary: 'ERROR',
              detail: 'Your authentication has expired',
            });
            throw new Error('401: Your authentication has expired');
          } else {
            this.messageservice.add({
              severity: 'error',
              summary: 'ERROR',
              detail: error.error,
            });
            throw new Error(error.status + ': ' + error.error);
          }
        })
      );
    }
  }
}
