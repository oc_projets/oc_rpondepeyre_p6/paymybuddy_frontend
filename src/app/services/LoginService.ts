import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Observable, Observer } from 'rxjs';
import { AuthRequest } from '../model/AuthRequestModel';
import { SubscriberModel } from '../model/SubscriberModel';
import { UserService } from './UserService';

export const ENV: string = 'http://localhost:8080/auth/';

@Injectable({
  providedIn: 'root',
})
export class LoginService {
  observer: Observer<boolean> = {
    next(value: boolean) {
      return value;
    },
    error: function (err: any): boolean {
      throw new Error(err.message);
    },
    complete: function (): void {},
  };

  constructor(
    private router: Router,
    private http: HttpClient,
    private userService: UserService
  ) {}

  login(request: AuthRequest): Observable<string> {
    return this.http.post(ENV + 'authent', request, {
      responseType: 'text',
    });
  }

  logout() {
    sessionStorage.clear();

    this.router.navigate(['login']);
  }

  verifToken(token: string, mail: string): Observable<boolean> {
    return this.http.post<boolean>(ENV + 'token', { token: token, mail: mail });
  }

  authenticate(token: string, mail: string) {
    if (this.verifToken(token, mail).subscribe(this.observer)) {
      sessionStorage.setItem('mail', mail);
      sessionStorage.setItem('token', token);

      this.router.navigate(['home']);
    }
  }

  verifyMail(subscribermail: string): Promise<boolean> {
    let param = new HttpParams().append('mail', subscribermail);
    return this.http.get<boolean>(ENV + 'exist', { params: param }).toPromise();
  }

  subscribe(subscriber: SubscriberModel): Promise<HttpResponse<string>> {
    return this.http
      .post(ENV + 'subscribe', subscriber, {
        responseType: 'text',
        observe: 'response',
      })
      .toPromise();
  }

  islogged(): any {
    const token = sessionStorage.getItem('token');
    const mail = sessionStorage.getItem('mail');
    if (
      token != null &&
      mail != null &&
      this.verifToken(token, mail).subscribe(this.observer)
    ) {
      return true;
    } else {
      return false;
    }
  }
}
