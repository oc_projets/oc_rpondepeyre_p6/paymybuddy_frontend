export interface UserModel {
  mail: string;
  lastName: string;
  firstName: string;
  fullName: string;
}
