import { UserModel } from './UserModel';

export interface TransactionModel {
  description?: string;
  montant: number;
  date?: Date;
  connection?: UserModel;
}
