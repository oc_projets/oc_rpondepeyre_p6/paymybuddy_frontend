export interface AccountModel {
  id?: number;
  nom: string;
  iban: string;
  bic: string;
}
