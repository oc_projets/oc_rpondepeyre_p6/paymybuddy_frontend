import { AccountModel } from './AccountModel';
import { TransactionModel } from './TransactionModel';
import { UserModel } from './UserModel';

export interface CurrentUserModel {
  id: number;
  mail: string;
  lastName: string;
  firstName: string;
  wallet: number;
  relations: Array<UserModel>;
  banques: Array<AccountModel>;
  transactions: Array<TransactionModel>;
}
