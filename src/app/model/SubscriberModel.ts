export interface SubscriberModel {
  mail: string;
  password: string;
  lastName: string;
  firstName: string;
}
