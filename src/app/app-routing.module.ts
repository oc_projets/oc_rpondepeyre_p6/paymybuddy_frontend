import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { TransferComponent } from './pages/transfer/transfer.component';
import { LoggedInGuard } from './services/logged-in.guard';
import { LoggedOutGuard } from './services/logged-out.guard';

const routes: Routes = [
  { path: 'home', component: HomeComponent, canActivate: [LoggedInGuard] },
  {
    path: 'transfer',
    component: TransferComponent,
    canActivate: [LoggedInGuard],
  },
  {
    path: 'profile',
    component: ProfileComponent,
    canActivate: [LoggedInGuard],
  },
  {
    path: 'contact',
    component: ContactComponent,
    canActivate: [LoggedInGuard],
  },
  { path: 'login', component: LoginComponent, canActivate: [LoggedOutGuard] },
  {
    path: 'subscribe',
    component: SubscribeComponent,
    canActivate: [LoggedOutGuard],
  },

  { path: '**', redirectTo: 'home' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
