import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { CurrentUserModel } from 'src/app/model/CurrentUserModel';
import { TransactionModel } from 'src/app/model/TransactionModel';
import { UserModel } from 'src/app/model/UserModel';
import { UserService } from 'src/app/services/UserService';

@Component({
  selector: 'app-transfer',
  templateUrl: './transfer.component.html',
  styleUrls: ['./transfer.component.scss'],
})
export class TransferComponent implements OnInit {
  items: Array<MenuItem> = [{ label: 'Transfer', routerLink: '/transfer' }];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/home' };
  amount: any;
  description: string;

  commission: number;

  newTransaction: TransactionModel = {
    description: undefined,
    montant: 0,
    date: undefined,
    connection: undefined,
  };
  filteredRelations: UserModel[];
  relation: UserModel;

  currentUser: CurrentUserModel = {
    id: 0,
    mail: '',
    lastName: '',
    firstName: '',
    relations: [],
    banques: [],
    wallet: 0,
    transactions: [],
  };

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}

  ngOnInit(): void {
    this.userService.currentUser.subscribe(value => (this.currentUser = value));
    this.userService.updateCurrentUser();
    this.userService
      .getCommission()
      .subscribe(value => (this.commission = value));
  }

  filterRelations(event: any) {
    //in a real application, make a request to a remote url with the query and return filtered results, for demo we filter at client side
    let filtered: UserModel[] = [];
    let query = event.query;

    for (const element of this.currentUser.relations) {
      let relation = element;
      if (relation.fullName.toLowerCase().indexOf(query.toLowerCase()) == 0) {
        filtered.push(relation);
      }
    }

    this.filteredRelations = filtered;
  }

  verifAmount(amount: number): boolean {
    if (amount < 0) {
      return true;
    } else {
      return false;
    }
  }

  transferMoney() {
    if (
      this.newTransaction.connection !== undefined &&
      this.newTransaction.montant !== 0 &&
      this.newTransaction.description !== undefined
    ) {
      let paid: number =
        this.newTransaction.montant +
        this.newTransaction.montant * (this.commission / 100);
      this.confirmationService.confirm({
        message: 'Your are going to pay ' + paid + ' $. Are you sure ?',
        accept: () => {
          this.userService
            .transferMoney(this.newTransaction)
            .subscribe((value: CurrentUserModel) => {
              this.userService.currentUser.next(value);
            });
        },
      });
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'All fields must be filled',
      });
    }
  }
}
