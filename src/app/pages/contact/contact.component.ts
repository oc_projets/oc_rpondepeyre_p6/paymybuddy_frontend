import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss'],
})
export class ContactComponent implements OnInit {
  items: Array<MenuItem> = [{ label: 'Contact', routerLink: '/contact' }];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/home' };

  constructor() {}

  ngOnInit(): void {}
}
