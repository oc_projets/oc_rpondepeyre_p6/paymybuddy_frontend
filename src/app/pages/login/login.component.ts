import { Component, OnInit } from '@angular/core';
import { MenuItem, MessageService } from 'primeng/api';
import { Observer } from 'rxjs';
import { AuthRequest } from 'src/app/model/AuthRequestModel';
import { LoginService } from 'src/app/services/LoginService';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  items: Array<MenuItem> = [{ label: 'Login', routerLink: '/login' }];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/home' };
  rememberMe: boolean;

  mail: string;
  password: string;

  constructor(
    private messageservice: MessageService,
    private loginService: LoginService
  ) {}

  ngOnInit(): void {}

  async login() {
    let request: AuthRequest = {
      username: this.mail,
      password: this.password,
    };

    const loginObserver: Observer<string> = {
      next: (response: string) => {
        this.loginService.authenticate(response, request.username);
        this.messageservice.add({
          severity: 'success',
          summary: 'Success',
          detail: 'Successfully logged in !',
        });
      },
      error: (response: any) => {
        this.messageservice.add({
          severity: 'error',
          summary: 'ERROR',
          detail: response.error,
        });
      },
      complete: function (): void {},
    };
    this.loginService.login(request).subscribe(loginObserver);
  }
}
