import { HttpResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { MessageService } from 'primeng/api';
import { SubscriberModel } from 'src/app/model/SubscriberModel';
import { LoginService } from 'src/app/services/LoginService';

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.scss'],
})
export class SubscribeComponent {
  subscriber: SubscriberModel = {
    mail: '',
    password: '',
    firstName: '',
    lastName: '',
  };

  constructor(
    private loginservice: LoginService,
    private messageservice: MessageService,
    private routerservice: Router
  ) {}

  async subscribe() {
    if (
      this.subscriber.firstName != '' &&
      this.subscriber.password != '' &&
      this.subscriber.lastName != '' &&
      this.subscriber.mail != ''
    ) {
      if (!(await this.loginservice.verifyMail(this.subscriber.mail))) {
        let response: HttpResponse<string> = await this.loginservice.subscribe(
          this.subscriber
        );
        if (response.status === 201) {
          this.messageservice.add({
            severity: 'success',
            summary: 'Success',
            detail: response.body!,
          });

          this.routerservice.navigateByUrl('/login');
        } else {
          this.messageservice.add({
            severity: 'error',
            summary: 'ERROR',
            detail: response.body!,
          });
        }
      } else {
        this.messageservice.add({
          severity: 'error',
          summary: 'ERROR',
          detail: 'E-mail already taken',
        });
      }
    } else {
      this.messageservice.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'All fields must be filled',
      });
    }
  }
}
