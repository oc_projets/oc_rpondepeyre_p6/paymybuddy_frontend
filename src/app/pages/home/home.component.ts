import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent {
  items: Array<MenuItem> = [];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/home' };
  constructor(private http: HttpClient) {}
}
