import { Component, OnInit } from '@angular/core';
import { ConfirmationService, MenuItem, MessageService } from 'primeng/api';
import { AccountModel } from 'src/app/model/AccountModel';
import { CurrentUserModel } from 'src/app/model/CurrentUserModel';
import { SubscriberModel } from 'src/app/model/SubscriberModel';
import { UserService } from 'src/app/services/UserService';

export enum radioButton {
  deposit = 'deposit',
  withdraw = 'withdraw',
}

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss'],
})
export class ProfileComponent implements OnInit {
  items: Array<MenuItem> = [{ label: 'Profile', routerLink: '/profile' }];
  home: MenuItem = { icon: 'pi pi-home', routerLink: '/home' };

  token = sessionStorage.getItem('token');

  oldPassword: string;
  newPassword: string;
  confirmPassword: string;

  selectedradio: radioButton;
  selectedbank: AccountModel = {
    nom: '',
    iban: '',
    bic: '',
  };
  amount: number;

  currentUser: CurrentUserModel = {
    id: 0,
    mail: '',
    lastName: '',
    firstName: '',
    relations: [],
    banques: [],
    wallet: 0,
    transactions: [],
  };

  searchMail: string;

  subscriber: SubscriberModel = {
    mail: '',
    password: '',
    firstName: '',
    lastName: '',
  };

  addWallet: AccountModel = {
    nom: '',
    iban: '',
    bic: '',
  };

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService,
    private messageService: MessageService
  ) {}
  ngOnInit(): void {
    this.userService.currentUser.subscribe(value => (this.currentUser = value));
    this.userService.updateCurrentUser();
  }

  addConnection() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to add this person ?',
      accept: () => {
        this.userService
          .addUserAsFriend(this.searchMail)
          .subscribe((value: CurrentUserModel) => {
            this.userService.currentUser.next(value);
          });
      },
    });
  }

  addBank() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to add this bank account ?',
      accept: () => {
        this.userService
          .addBankAccount(this.addWallet)
          .subscribe((value: CurrentUserModel) => {
            this.userService.currentUser.next(value);
          });
      },
    });
  }

  transferMoney() {
    if (this.amount != 0 && this.amount != undefined) {
      if (this.selectedradio != undefined) {
        if (this.selectedradio == radioButton.deposit) {
          this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this?',
            accept: () => {
              this.userService
                .deposit(this.selectedbank, this.amount)
                .subscribe((value: CurrentUserModel) => {
                  this.userService.currentUser.next(value);
                });
            },
          });
        } else if (this.selectedradio == radioButton.withdraw) {
          this.confirmationService.confirm({
            message: 'Are you sure that you want to perform this?',
            accept: () => {
              this.userService
                .withdraw(this.selectedbank, this.amount)
                .subscribe((value: CurrentUserModel) => {
                  this.userService.currentUser.next(value);
                });
            },
          });
        } else {
          this.messageService.add({
            severity: 'error',
            summary: 'ERROR',
            detail: 'An error Occured please try again',
          });
        }
      } else {
        this.messageService.add({
          severity: 'error',
          summary: 'ERROR',
          detail:
            'Please specify if you want to withdraw or deposit to perform',
        });
      }
    } else {
      this.messageService.add({
        severity: 'error',
        summary: 'ERROR',
        detail: 'Please specify an amount to perform',
      });
    }
  }
}
