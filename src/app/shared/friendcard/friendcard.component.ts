import { Component, Input } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { UserModel } from 'src/app/model/UserModel';
import { UserService } from 'src/app/services/UserService';

@Component({
  selector: 'app-friendcard',
  templateUrl: './friendcard.component.html',
  styleUrls: ['./friendcard.component.scss'],
})
export class FriendcardComponent {
  @Input() user: UserModel;

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService
  ) {}

  removeFriend() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to remove this connection ?',
      accept: () => {
        this.userService
          .removeFriend(this.user.mail)
          .subscribe(value => this.userService.currentUser.next(value));
      },
    });
  }
}
