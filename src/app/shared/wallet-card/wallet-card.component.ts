import { Component, Input } from '@angular/core';
import { ConfirmationService } from 'primeng/api';
import { AccountModel } from 'src/app/model/AccountModel';
import { UserService } from 'src/app/services/UserService';

@Component({
  selector: 'app-wallet-card',
  templateUrl: './wallet-card.component.html',
  styleUrls: ['./wallet-card.component.scss'],
})
export class WalletCardComponent {
  @Input() wallet: AccountModel;

  constructor(
    private userService: UserService,
    private confirmationService: ConfirmationService
  ) {}

  removeBank() {
    this.confirmationService.confirm({
      message: 'Are you sure that you want to remove this connection ?',
      accept: () => {
        this.userService
          .removeBankAccount(this.wallet.id!)
          .subscribe(value => this.userService.currentUser.next(value));
      },
    });
  }
}
