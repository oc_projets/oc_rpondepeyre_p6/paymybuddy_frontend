import { Component } from '@angular/core';
import { LoginService } from './services/LoginService';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'paymybuddy-angular';

  constructor(private loginService: LoginService) {}
}
