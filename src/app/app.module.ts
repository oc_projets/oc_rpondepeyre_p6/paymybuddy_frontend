import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';

import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ConfirmationService, MessageService } from 'primeng/api';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { BreadcrumbModule } from 'primeng/breadcrumb';
import { ButtonModule } from 'primeng/button';
import { CheckboxModule } from 'primeng/checkbox';
import { ConfirmDialogModule } from 'primeng/confirmdialog';
import { DropdownModule } from 'primeng/dropdown';
import { InputNumberModule } from 'primeng/inputnumber';
import { InputTextModule } from 'primeng/inputtext';
import { PasswordModule } from 'primeng/password';
import { TableModule } from 'primeng/table';
import { ToastModule } from 'primeng/toast';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ContactComponent } from './pages/contact/contact.component';
import { HomeComponent } from './pages/home/home.component';
import { LoginComponent } from './pages/login/login.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { SubscribeComponent } from './pages/subscribe/subscribe.component';
import { TransferComponent } from './pages/transfer/transfer.component';
import { AuthInterceptor } from './services/auth-interceptor';
import { FriendcardComponent } from './shared/friendcard/friendcard.component';
import { HeaderComponent } from './shared/header/header.component';
import { UserformComponent } from './shared/userform/userform.component';
import { WalletCardComponent } from './shared/wallet-card/wallet-card.component';

import { RadioButtonModule } from 'primeng/radiobutton';

@NgModule({
  declarations: [
    AppComponent,
    TransferComponent,
    HeaderComponent,
    HomeComponent,
    ProfileComponent,
    ContactComponent,
    LoginComponent,
    SubscribeComponent,
    UserformComponent,
    FriendcardComponent,
    WalletCardComponent,
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    ButtonModule,
    BreadcrumbModule,
    TableModule,
    DropdownModule,
    AutoCompleteModule,
    InputTextModule,
    CheckboxModule,
    PasswordModule,
    ToastModule,
    ConfirmDialogModule,
    InputNumberModule,
    RadioButtonModule,
  ],
  providers: [
    ConfirmationService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptor,
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
