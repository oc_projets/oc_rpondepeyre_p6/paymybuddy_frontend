Pour lancer le projet:

1/ Installez une version recente de NodeJs
2/ Clonez et ouvrez le projet dans l'IDE de votre choix
3/Lancez la commande "npm install" pour installer les packages requis
4/ Lancez la commande "npm start" pour lancer le serveur
5/ L'application est disponible a l'addresse localhost:4200
